import React from 'react';
import { shallow } from 'enzyme';
import List from './list';

describe('<List />', () => {
  test('renders', () => {
    const wrapper = shallow(<List />);
    expect(wrapper).toMatchSnapshot();
  });
});
