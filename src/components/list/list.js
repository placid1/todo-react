import React, { Component } from 'react';
import './list.css';
import {Bounce} from 'react-reveal';
import {Alert} from 'react-bootstrap';

class List extends Component {
  constructor(props){
    super(props)
  }
  render() {
    const {onDeleteItem, todos} = this.props;
    let _list;
    if (todos.length === 0) {
      _list = <p className="text-center">There is nothing ToDo</p>
    } else {
    _list = todos.map(item => {
      return (
        <Bounce key={item.id} up>
          <Alert variant='primary' className={item.done?'linethrought':'line-none'} onClick={()=>onDeleteItem(item)}>
            {item.title}
          </Alert>
        </Bounce>
      )
    })
  }
    return (
      <React.Fragment>
        {_list}
      </React.Fragment>
    );
  }
}

export default List;
