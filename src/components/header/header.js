import React, { Component, Fragment } from 'react';
import {ProgressBar} from 'react-bootstrap';

const Header = ({ completed, children }) => {
    return <React.Fragment>
      <ProgressBar now={completed}/>
      <div className="text-center py-4">
        <h1>
          {children}
        </h1>
      </div>
    </React.Fragment>
}

export default Header;
