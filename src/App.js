import React, {Component} from 'react';
import 'bootstrap/dist/css/bootstrap.min.css';
import Header from './components/header/header';
import List from './components/list/list'
import {Form, Container, Alert, Row, Col, Button, Modal} from 'react-bootstrap';

class App extends Component {
  constructor(){
    super()
    this.state = {
      input: '',
      modalShow:false,
      alert: {
        message: "",
        show: false,
        type:'info'
      },
      todos:[
        
      ],
      completed: 0
    }

  }

  addItem = (event) => {
    const todos = this.state.input!==''?[...this.state.todos, {id: Math.random(), title:this.state.input, done:false}]:[...this.state.todos]
    this.setState({
      todos,
    },()=>{
      this.state.input===''?this.showAlert('Please enter something','danger'):this.showAlert('Task added!', 'success');
      this.completedTasks();
    });
  }

  showAlert = (message, type) => {
    this.setState({
      alert: {
        message,
        type,
        show:true
      },
      input: ''
    }, () => {
      setTimeout(()=> {
        this.setState({
          alert: {
            show: true,
          }
        })
      }, 1200)
    })
  }

  deleteItem = (item) => {
    const {todos} = this.state;
    const i = todos.indexOf(item);
    todos[i].done = !todos[i].done;
    this.setState({
      todos
    }, this.completedTasks)
  }

  completedTasks = () => {
    const {todos} = this.state;
    const todosLength = todos.length;
    const todosCompleted = todos.filter(item=>item.done).length;
    const completed = todosCompleted!==0?Math.ceil(todosCompleted/todosLength*100):0;
    this.setState({
      completed
    })
  }

  render(){
    const {todos, input, alert, modalShow} = this.state;
    return (
      <React.Fragment>
        <Header completed={this.state.completed}>What ToDo?</Header>
        <Container>

      <Modal show={modalShow} onHide={()=>{this.setState({modalShow:false})}}>
        <Modal.Body className="py-5">
          <Row>
          <Col>
              <Alert show={alert.show} variant={alert.type}>
                {alert.message}
              </Alert>

            </Col>
            <Col lg={12}>
              <Form onSubmit={(event)=> {event.preventDefault(); this.addItem()}}>
                <Form.Group>
                  <Form.Label>What needs to be done:</Form.Label>
                  <Form.Control placeholder="" onChange={(e)=>this.setState({input: e.target.value})} value={input}/>
                </Form.Group>
              </Form>
            </Col>
          </Row>
        </Modal.Body>
        <Modal.Footer>
          <Button variant="outline-primary" onClick={()=>{this.setState({modalShow:false})}}>
            Close
          </Button>
        </Modal.Footer>
      </Modal>
      {!modalShow?<Button variant="outline-primary" className="mb-4 w-100" onClick={()=>{this.setState({modalShow:true})}}>Add</Button>:null}
            <List todos={todos} onDeleteItem={this.deleteItem}></List>
            {todos.length?<Button variant="outline-danger" className="mb-4 w-100" onClick={()=>{this.setState({todos:[], completed: 0})}}>Clear All</Button>:null}
        </Container>
      </React.Fragment>
    );
  }
}

export default App;
